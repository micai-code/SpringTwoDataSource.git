/**
 * 
 */
package com.micai.spring.service.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.micai.spring.first.po.User;
import com.micai.spring.first.service.UserService;

/**
 * Spring的测试类编写方式 <br/>
 * 
 * @author ZhaoXinGuo
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class UserServiceTest {
	
	private final Logger logger = LoggerFactory.getLogger(UserServiceTest.class);

	@Autowired
	private UserService userService;

	@Test
	public void add() {
		User user = new User();
		user.setUserName("admin");
		user.setPassWord("admin");
		userService.saveUser(user);
	}
	
	@Test
	public void list(){
		List<User> list = userService.list();
		logger.info("用户信息为：" + list + "---------------------------");
	}
	
}
