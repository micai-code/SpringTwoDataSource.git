/**
 * 
 */
package com.micai.spring.second.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.micai.spring.base.dao.impl.BaseDaoImpl;
import com.micai.spring.second.dao.RoleDao;
import com.micai.spring.second.po.Role;

/**
 * @author ZhaoXinGuo
 * 
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

	public void saveRole(Role role) {
		this.add(role);
	}

	public List<Role> list() {
		String hql = "from Role";
		return this.getSession().createQuery(hql).list();
	}

}
