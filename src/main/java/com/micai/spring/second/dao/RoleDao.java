/**
 * 
 */
package com.micai.spring.second.dao;

import java.util.List;

import com.micai.spring.base.dao.BaseDao;
import com.micai.spring.second.po.Role;

/**
 * @author ZhaoXinGuo
 *
 */
public interface RoleDao extends BaseDao<Role>{

	void saveRole(Role role);

	List<Role> list();

}
