/**
 * 
 */
package com.micai.spring.second.service;

import java.util.List;

import com.micai.spring.second.po.Role;

/**
 * @author ZhaoXinGuo
 *
 */
public interface RoleService {

	void saveRole(Role role);

	List<Role> list();

}
