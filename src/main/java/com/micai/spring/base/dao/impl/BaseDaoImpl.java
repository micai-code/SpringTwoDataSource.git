/**
 * 
 */
package com.micai.spring.base.dao.impl;

import java.lang.reflect.ParameterizedType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.micai.spring.base.dao.BaseDao;

/**
 * @author XinGuo
 * 
 * @email sxdtzhaoxinguo@163.com
 */
public class BaseDaoImpl<T> implements BaseDao<T> {

	private SessionFactory sessionFactory;

	/**
	 * 创建一个Class的对象来获取泛型的class
	 */
	private Class<?> clz;

	public Class<?> getClz() {
		if (clz == null) {
			// 获取泛型的Class对象
			clz = ((Class<?>) (((ParameterizedType) (this.getClass()
					.getGenericSuperclass())).getActualTypeArguments()[0]));
		}
		return clz;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * 添加 TODO 添加override说明
	 * 
	 * @see com.qdexam.qdks.dao.IBaseDao#add(java.lang.Object)
	 */
	public T add(T t) {
		getSession().save(t);
		return t;
	}

	/**
	 * 修改 TODO 添加override说明
	 * 
	 * @see com.qdexam.qdks.dao.IBaseDao#update(java.lang.Object)
	 */
	public void update(T t) {
		getSession().update(t);
	}

	/**
	 * 删除 TODO 添加override说明
	 * 
	 * @see com.qdexam.qdks.dao.IBaseDao#delete(int)
	 */
	public void delete(int id) {
		getSession().delete(this.load(id));
	}

	/**
	 * 根据id查询对象 TODO 添加override说明
	 * 
	 * @see com.qdexam.qdks.dao.IBaseDao#load(int)
	 */
	public T load(int id) {
		return (T) getSession().load(getClz(), id);
	}

	public T get(int id) {
		return (T) getSession().get(getClz(), id);
	}

	/**
	 * 根据id查询对象 TODO 添加override说明
	 * 
	 * @see com.qdexam.qdks.dao.IBaseDao#load(java.lang.String)
	 */
	public T load(String id) {
		return (T) getSession().load(getClz(), id);
	}

}
