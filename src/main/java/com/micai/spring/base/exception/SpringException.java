/**
 * 
 */

package com.micai.spring.base.exception;

/**
 * @author ZhaoXinGuo
 * 
 * @email sxdtzhaoxinguo@163.com
 */
public class SpringException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpringException() {
		super();
	}

	public SpringException(String message, Throwable cause) {
		super(message, cause);
	}

	public SpringException(String message) {
		super(message);
	}

	public SpringException(Throwable cause) {
		super(cause);
	}

}
