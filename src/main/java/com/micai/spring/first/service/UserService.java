/**
 * 
 */
package com.micai.spring.first.service;

import java.util.List;
import com.micai.spring.first.po.User;

/**
 * @author ZhaoXinGuo
 *
 * @email sxdtzhaoxinguo@163.com
 */
public interface UserService{

	void saveUser(User user);

	List<User> list();
	
}
