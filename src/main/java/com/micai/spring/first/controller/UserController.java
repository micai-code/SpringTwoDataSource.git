/**
 * 
 */
package com.micai.spring.first.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.micai.spring.first.po.User;
import com.micai.spring.first.service.UserService;

/**
 * @author ZhaoXinGuo
 *
 * @email sxdtzhaoxinguo@163.com
 */
@Controller
public class UserController{
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/save")
	public void add(User user){
		userService.saveUser(user);
	}
}
