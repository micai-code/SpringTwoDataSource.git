/**
 * 
 */
package com.micai.spring.first.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.micai.spring.base.dao.impl.BaseDaoImpl;
import com.micai.spring.first.dao.UserDao;
import com.micai.spring.first.po.User;

/**
 * @author ZhaoXinGuo
 *
 * @email sxdtzhaoxinguo@163.com
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{

	public void saveUser(User user) {
		this.add(user);
	}

	public List<User> list() {
		String hql = "from User";
		List<User> list = this.getSession().createQuery(hql).list();
		return list;
	}

	
}
