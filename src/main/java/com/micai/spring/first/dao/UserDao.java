/**
 * 
 */
package com.micai.spring.first.dao;

import java.util.List;

import com.micai.spring.base.dao.BaseDao;
import com.micai.spring.first.po.User;

/**
 * @author ZhaoXinGuo
 *
 * @email sxdtzhaoxinguo@163.com
 */
public interface UserDao extends BaseDao<User>{

	void saveUser(User user);

	List<User> list();
	
}
